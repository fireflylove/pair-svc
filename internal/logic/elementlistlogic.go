package logic

import (
	"context"

	"gitee.com/fireflylove/pair-svc/internal/svc"
	"gitee.com/fireflylove/pair-svc/pair"

	"github.com/tal-tech/go-zero/core/logx"
)

type ElementListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewElementListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ElementListLogic {
	return &ElementListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *ElementListLogic) ElementList(in *pair.EleListReq) (*pair.ELeListRsp, error) {
	// todo: add your logic here and delete this line

	return &pair.ELeListRsp{}, nil
}
