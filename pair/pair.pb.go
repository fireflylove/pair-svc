// Code generated by protoc-gen-go. DO NOT EDIT.
// source: pair.proto

package pair

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type ELeListRsp struct {
	Code                 int64          `protobuf:"varint,1,opt,name=Code,proto3" json:"Code,omitempty"`
	Message              string         `protobuf:"bytes,2,opt,name=Message,proto3" json:"Message,omitempty"`
	UserElement          []*UserElement `protobuf:"bytes,3,rep,name=UserElement,proto3" json:"UserElement,omitempty"`
	Page                 int64          `protobuf:"varint,4,opt,name=Page,proto3" json:"Page,omitempty"`
	PageSize             int64          `protobuf:"varint,5,opt,name=PageSize,proto3" json:"PageSize,omitempty"`
	Total                int64          `protobuf:"varint,6,opt,name=Total,proto3" json:"Total,omitempty"`
	XXX_NoUnkeyedLiteral struct{}       `json:"-"`
	XXX_unrecognized     []byte         `json:"-"`
	XXX_sizecache        int32          `json:"-"`
}

func (m *ELeListRsp) Reset()         { *m = ELeListRsp{} }
func (m *ELeListRsp) String() string { return proto.CompactTextString(m) }
func (*ELeListRsp) ProtoMessage()    {}
func (*ELeListRsp) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{0}
}

func (m *ELeListRsp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ELeListRsp.Unmarshal(m, b)
}
func (m *ELeListRsp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ELeListRsp.Marshal(b, m, deterministic)
}
func (m *ELeListRsp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ELeListRsp.Merge(m, src)
}
func (m *ELeListRsp) XXX_Size() int {
	return xxx_messageInfo_ELeListRsp.Size(m)
}
func (m *ELeListRsp) XXX_DiscardUnknown() {
	xxx_messageInfo_ELeListRsp.DiscardUnknown(m)
}

var xxx_messageInfo_ELeListRsp proto.InternalMessageInfo

func (m *ELeListRsp) GetCode() int64 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *ELeListRsp) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *ELeListRsp) GetUserElement() []*UserElement {
	if m != nil {
		return m.UserElement
	}
	return nil
}

func (m *ELeListRsp) GetPage() int64 {
	if m != nil {
		return m.Page
	}
	return 0
}

func (m *ELeListRsp) GetPageSize() int64 {
	if m != nil {
		return m.PageSize
	}
	return 0
}

func (m *ELeListRsp) GetTotal() int64 {
	if m != nil {
		return m.Total
	}
	return 0
}

type EleListReq struct {
	Page                 int64    `protobuf:"varint,1,opt,name=Page,proto3" json:"Page,omitempty"`
	PageSize             int64    `protobuf:"varint,2,opt,name=PageSize,proto3" json:"PageSize,omitempty"`
	Keyword              string   `protobuf:"bytes,3,opt,name=keyword,proto3" json:"keyword,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EleListReq) Reset()         { *m = EleListReq{} }
func (m *EleListReq) String() string { return proto.CompactTextString(m) }
func (*EleListReq) ProtoMessage()    {}
func (*EleListReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{1}
}

func (m *EleListReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EleListReq.Unmarshal(m, b)
}
func (m *EleListReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EleListReq.Marshal(b, m, deterministic)
}
func (m *EleListReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EleListReq.Merge(m, src)
}
func (m *EleListReq) XXX_Size() int {
	return xxx_messageInfo_EleListReq.Size(m)
}
func (m *EleListReq) XXX_DiscardUnknown() {
	xxx_messageInfo_EleListReq.DiscardUnknown(m)
}

var xxx_messageInfo_EleListReq proto.InternalMessageInfo

func (m *EleListReq) GetPage() int64 {
	if m != nil {
		return m.Page
	}
	return 0
}

func (m *EleListReq) GetPageSize() int64 {
	if m != nil {
		return m.PageSize
	}
	return 0
}

func (m *EleListReq) GetKeyword() string {
	if m != nil {
		return m.Keyword
	}
	return ""
}

type ElePairReq struct {
	Uid                  uint64   `protobuf:"varint,1,opt,name=Uid,proto3" json:"Uid,omitempty"`
	Page                 int64    `protobuf:"varint,2,opt,name=Page,proto3" json:"Page,omitempty"`
	PageSize             int64    `protobuf:"varint,3,opt,name=PageSize,proto3" json:"PageSize,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ElePairReq) Reset()         { *m = ElePairReq{} }
func (m *ElePairReq) String() string { return proto.CompactTextString(m) }
func (*ElePairReq) ProtoMessage()    {}
func (*ElePairReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{2}
}

func (m *ElePairReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ElePairReq.Unmarshal(m, b)
}
func (m *ElePairReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ElePairReq.Marshal(b, m, deterministic)
}
func (m *ElePairReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ElePairReq.Merge(m, src)
}
func (m *ElePairReq) XXX_Size() int {
	return xxx_messageInfo_ElePairReq.Size(m)
}
func (m *ElePairReq) XXX_DiscardUnknown() {
	xxx_messageInfo_ElePairReq.DiscardUnknown(m)
}

var xxx_messageInfo_ElePairReq proto.InternalMessageInfo

func (m *ElePairReq) GetUid() uint64 {
	if m != nil {
		return m.Uid
	}
	return 0
}

func (m *ElePairReq) GetPage() int64 {
	if m != nil {
		return m.Page
	}
	return 0
}

func (m *ElePairReq) GetPageSize() int64 {
	if m != nil {
		return m.PageSize
	}
	return 0
}

type EleViewRsp struct {
	Code                 int64      `protobuf:"varint,1,opt,name=Code,proto3" json:"Code,omitempty"`
	Message              string     `protobuf:"bytes,2,opt,name=Message,proto3" json:"Message,omitempty"`
	Element              []*Element `protobuf:"bytes,3,rep,name=Element,proto3" json:"Element,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *EleViewRsp) Reset()         { *m = EleViewRsp{} }
func (m *EleViewRsp) String() string { return proto.CompactTextString(m) }
func (*EleViewRsp) ProtoMessage()    {}
func (*EleViewRsp) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{3}
}

func (m *EleViewRsp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EleViewRsp.Unmarshal(m, b)
}
func (m *EleViewRsp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EleViewRsp.Marshal(b, m, deterministic)
}
func (m *EleViewRsp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EleViewRsp.Merge(m, src)
}
func (m *EleViewRsp) XXX_Size() int {
	return xxx_messageInfo_EleViewRsp.Size(m)
}
func (m *EleViewRsp) XXX_DiscardUnknown() {
	xxx_messageInfo_EleViewRsp.DiscardUnknown(m)
}

var xxx_messageInfo_EleViewRsp proto.InternalMessageInfo

func (m *EleViewRsp) GetCode() int64 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *EleViewRsp) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

func (m *EleViewRsp) GetElement() []*Element {
	if m != nil {
		return m.Element
	}
	return nil
}

type EleViewReq struct {
	Uid                  uint64   `protobuf:"varint,1,opt,name=Uid,proto3" json:"Uid,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *EleViewReq) Reset()         { *m = EleViewReq{} }
func (m *EleViewReq) String() string { return proto.CompactTextString(m) }
func (*EleViewReq) ProtoMessage()    {}
func (*EleViewReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{4}
}

func (m *EleViewReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EleViewReq.Unmarshal(m, b)
}
func (m *EleViewReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EleViewReq.Marshal(b, m, deterministic)
}
func (m *EleViewReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EleViewReq.Merge(m, src)
}
func (m *EleViewReq) XXX_Size() int {
	return xxx_messageInfo_EleViewReq.Size(m)
}
func (m *EleViewReq) XXX_DiscardUnknown() {
	xxx_messageInfo_EleViewReq.DiscardUnknown(m)
}

var xxx_messageInfo_EleViewReq proto.InternalMessageInfo

func (m *EleViewReq) GetUid() uint64 {
	if m != nil {
		return m.Uid
	}
	return 0
}

type EleSaveReq struct {
	Uid                  uint64     `protobuf:"varint,1,opt,name=Uid,proto3" json:"Uid,omitempty"`
	Element              []*Element `protobuf:"bytes,2,rep,name=Element,proto3" json:"Element,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *EleSaveReq) Reset()         { *m = EleSaveReq{} }
func (m *EleSaveReq) String() string { return proto.CompactTextString(m) }
func (*EleSaveReq) ProtoMessage()    {}
func (*EleSaveReq) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{5}
}

func (m *EleSaveReq) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_EleSaveReq.Unmarshal(m, b)
}
func (m *EleSaveReq) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_EleSaveReq.Marshal(b, m, deterministic)
}
func (m *EleSaveReq) XXX_Merge(src proto.Message) {
	xxx_messageInfo_EleSaveReq.Merge(m, src)
}
func (m *EleSaveReq) XXX_Size() int {
	return xxx_messageInfo_EleSaveReq.Size(m)
}
func (m *EleSaveReq) XXX_DiscardUnknown() {
	xxx_messageInfo_EleSaveReq.DiscardUnknown(m)
}

var xxx_messageInfo_EleSaveReq proto.InternalMessageInfo

func (m *EleSaveReq) GetUid() uint64 {
	if m != nil {
		return m.Uid
	}
	return 0
}

func (m *EleSaveReq) GetElement() []*Element {
	if m != nil {
		return m.Element
	}
	return nil
}

type Response struct {
	Code                 int64    `protobuf:"varint,1,opt,name=Code,proto3" json:"Code,omitempty"`
	Message              string   `protobuf:"bytes,2,opt,name=Message,proto3" json:"Message,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Response) Reset()         { *m = Response{} }
func (m *Response) String() string { return proto.CompactTextString(m) }
func (*Response) ProtoMessage()    {}
func (*Response) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{6}
}

func (m *Response) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Response.Unmarshal(m, b)
}
func (m *Response) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Response.Marshal(b, m, deterministic)
}
func (m *Response) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Response.Merge(m, src)
}
func (m *Response) XXX_Size() int {
	return xxx_messageInfo_Response.Size(m)
}
func (m *Response) XXX_DiscardUnknown() {
	xxx_messageInfo_Response.DiscardUnknown(m)
}

var xxx_messageInfo_Response proto.InternalMessageInfo

func (m *Response) GetCode() int64 {
	if m != nil {
		return m.Code
	}
	return 0
}

func (m *Response) GetMessage() string {
	if m != nil {
		return m.Message
	}
	return ""
}

type UserElement struct {
	Id                   uint64     `protobuf:"varint,1,opt,name=Id,proto3" json:"Id,omitempty"`
	Name                 string     `protobuf:"bytes,2,opt,name=Name,proto3" json:"Name,omitempty"`
	Avatar               string     `protobuf:"bytes,3,opt,name=Avatar,proto3" json:"Avatar,omitempty"`
	Account              string     `protobuf:"bytes,4,opt,name=Account,proto3" json:"Account,omitempty"`
	Element              []*Element `protobuf:"bytes,5,rep,name=Element,proto3" json:"Element,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *UserElement) Reset()         { *m = UserElement{} }
func (m *UserElement) String() string { return proto.CompactTextString(m) }
func (*UserElement) ProtoMessage()    {}
func (*UserElement) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{7}
}

func (m *UserElement) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UserElement.Unmarshal(m, b)
}
func (m *UserElement) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UserElement.Marshal(b, m, deterministic)
}
func (m *UserElement) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UserElement.Merge(m, src)
}
func (m *UserElement) XXX_Size() int {
	return xxx_messageInfo_UserElement.Size(m)
}
func (m *UserElement) XXX_DiscardUnknown() {
	xxx_messageInfo_UserElement.DiscardUnknown(m)
}

var xxx_messageInfo_UserElement proto.InternalMessageInfo

func (m *UserElement) GetId() uint64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *UserElement) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *UserElement) GetAvatar() string {
	if m != nil {
		return m.Avatar
	}
	return ""
}

func (m *UserElement) GetAccount() string {
	if m != nil {
		return m.Account
	}
	return ""
}

func (m *UserElement) GetElement() []*Element {
	if m != nil {
		return m.Element
	}
	return nil
}

type Element struct {
	Uid                  uint64   `protobuf:"varint,1,opt,name=Uid,proto3" json:"Uid,omitempty"`
	Name                 string   `protobuf:"bytes,2,opt,name=Name,proto3" json:"Name,omitempty"`
	Mode                 string   `protobuf:"bytes,3,opt,name=Mode,proto3" json:"Mode,omitempty"`
	Star                 bool     `protobuf:"varint,4,opt,name=Star,proto3" json:"Star,omitempty"`
	Sort                 int64    `protobuf:"varint,5,opt,name=Sort,proto3" json:"Sort,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Element) Reset()         { *m = Element{} }
func (m *Element) String() string { return proto.CompactTextString(m) }
func (*Element) ProtoMessage()    {}
func (*Element) Descriptor() ([]byte, []int) {
	return fileDescriptor_b6c646fab57af36d, []int{8}
}

func (m *Element) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Element.Unmarshal(m, b)
}
func (m *Element) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Element.Marshal(b, m, deterministic)
}
func (m *Element) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Element.Merge(m, src)
}
func (m *Element) XXX_Size() int {
	return xxx_messageInfo_Element.Size(m)
}
func (m *Element) XXX_DiscardUnknown() {
	xxx_messageInfo_Element.DiscardUnknown(m)
}

var xxx_messageInfo_Element proto.InternalMessageInfo

func (m *Element) GetUid() uint64 {
	if m != nil {
		return m.Uid
	}
	return 0
}

func (m *Element) GetName() string {
	if m != nil {
		return m.Name
	}
	return ""
}

func (m *Element) GetMode() string {
	if m != nil {
		return m.Mode
	}
	return ""
}

func (m *Element) GetStar() bool {
	if m != nil {
		return m.Star
	}
	return false
}

func (m *Element) GetSort() int64 {
	if m != nil {
		return m.Sort
	}
	return 0
}

func init() {
	proto.RegisterType((*ELeListRsp)(nil), "pair.ELeListRsp")
	proto.RegisterType((*EleListReq)(nil), "pair.EleListReq")
	proto.RegisterType((*ElePairReq)(nil), "pair.ElePairReq")
	proto.RegisterType((*EleViewRsp)(nil), "pair.EleViewRsp")
	proto.RegisterType((*EleViewReq)(nil), "pair.EleViewReq")
	proto.RegisterType((*EleSaveReq)(nil), "pair.EleSaveReq")
	proto.RegisterType((*Response)(nil), "pair.Response")
	proto.RegisterType((*UserElement)(nil), "pair.UserElement")
	proto.RegisterType((*Element)(nil), "pair.Element")
}

func init() { proto.RegisterFile("pair.proto", fileDescriptor_b6c646fab57af36d) }

var fileDescriptor_b6c646fab57af36d = []byte{
	// 438 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x94, 0x54, 0x4b, 0x8b, 0xdb, 0x30,
	0x10, 0xc6, 0x8f, 0x3c, 0x76, 0x42, 0x97, 0xad, 0x28, 0x45, 0xe4, 0x50, 0x82, 0x2f, 0xcd, 0x69,
	0x4b, 0x77, 0x2f, 0xbd, 0x2e, 0x25, 0x94, 0x85, 0x4d, 0x08, 0x4a, 0x93, 0xbb, 0x9a, 0x88, 0x62,
	0xf2, 0x90, 0x2b, 0xb9, 0x09, 0xed, 0x6f, 0xe8, 0xdf, 0xe9, 0xff, 0xe8, 0x4f, 0x2a, 0x33, 0x96,
	0x65, 0xe7, 0x09, 0x39, 0x65, 0x66, 0xe4, 0xef, 0x31, 0x33, 0x52, 0x00, 0x32, 0x99, 0x9a, 0xfb,
	0xcc, 0xe8, 0x5c, 0xb3, 0x18, 0xe3, 0xe4, 0x6f, 0x00, 0x30, 0x78, 0x51, 0x2f, 0xa9, 0xcd, 0x85,
	0xcd, 0x18, 0x83, 0xf8, 0xb3, 0x5e, 0x28, 0x1e, 0xf4, 0x82, 0x7e, 0x24, 0x28, 0x66, 0x1c, 0x5a,
	0x43, 0x65, 0xad, 0xfc, 0xae, 0x78, 0xd8, 0x0b, 0xfa, 0x37, 0xa2, 0x4c, 0xd9, 0x23, 0x74, 0xa6,
	0x56, 0x99, 0xc1, 0x4a, 0xad, 0xd5, 0x26, 0xe7, 0x51, 0x2f, 0xea, 0x77, 0x1e, 0x5e, 0xdf, 0x93,
	0x48, 0xed, 0x40, 0xd4, 0xbf, 0x42, 0x89, 0x31, 0x72, 0xc5, 0x85, 0x04, 0xc6, 0xac, 0x0b, 0x6d,
	0xfc, 0x9d, 0xa4, 0xbf, 0x15, 0x6f, 0x50, 0xdd, 0xe7, 0xec, 0x0d, 0x34, 0xbe, 0xea, 0x5c, 0xae,
	0x78, 0x93, 0x0e, 0x8a, 0x24, 0x99, 0x01, 0x0c, 0x56, 0x85, 0x6d, 0xf5, 0xc3, 0x73, 0x06, 0x67,
	0x38, 0xc3, 0x03, 0x4e, 0x0e, 0xad, 0xa5, 0xfa, 0xb5, 0xd3, 0x66, 0xc1, 0xa3, 0xa2, 0x25, 0x97,
	0x26, 0x23, 0xe2, 0x1d, 0xcb, 0xd4, 0x20, 0xef, 0x1d, 0x44, 0xd3, 0x74, 0x41, 0xb4, 0xb1, 0xc0,
	0xd0, 0x2b, 0x85, 0x67, 0x94, 0xa2, 0x7d, 0xa5, 0x64, 0x4e, 0x7c, 0xb3, 0x54, 0xed, 0xae, 0x1f,
	0xef, 0x7b, 0x68, 0xed, 0x8f, 0xf6, 0x55, 0x31, 0xda, 0x72, 0xac, 0xe5, 0x69, 0xf2, 0xae, 0x12,
	0x39, 0x65, 0x3a, 0xf9, 0x42, 0xe7, 0x13, 0xb9, 0x55, 0xa7, 0x9b, 0xaa, 0x09, 0x85, 0x17, 0x85,
	0x3e, 0x41, 0x5b, 0x28, 0x9b, 0xe9, 0x8d, 0x55, 0xd7, 0xf5, 0x92, 0xfc, 0x09, 0xf6, 0xee, 0x0a,
	0xbb, 0x85, 0xf0, 0xb9, 0xf4, 0x10, 0x3e, 0xd3, 0x5c, 0x47, 0x72, 0x5d, 0xc2, 0x28, 0x66, 0x6f,
	0xa1, 0xf9, 0xb4, 0x95, 0xb9, 0x34, 0x6e, 0x49, 0x2e, 0x43, 0x95, 0xa7, 0xf9, 0x5c, 0xff, 0xdc,
	0xe4, 0x74, 0x89, 0x6e, 0x44, 0x99, 0xd6, 0x1b, 0x69, 0x5c, 0x6c, 0x64, 0xe9, 0x3f, 0x3c, 0xbd,
	0xe3, 0x23, 0x2f, 0x0c, 0xe2, 0x21, 0x76, 0x5b, 0x38, 0xa1, 0x18, 0x6b, 0x13, 0x74, 0x87, 0x26,
	0xda, 0x82, 0x62, 0xaa, 0x69, 0x93, 0xbb, 0x5b, 0x4c, 0xf1, 0xc3, 0xbf, 0x00, 0x2f, 0x4d, 0x6a,
	0xd8, 0x07, 0xe8, 0x38, 0x55, 0xdc, 0x05, 0xbb, 0xf3, 0xe6, 0xdc, 0x6a, 0xba, 0xb7, 0x45, 0xc5,
	0xcf, 0xf8, 0xa3, 0x07, 0xe0, 0x72, 0x6b, 0x00, 0xb7, 0xeb, 0xee, 0x41, 0xc5, 0x66, 0x35, 0x08,
	0x3e, 0x8e, 0x1a, 0xc4, 0xbd, 0x15, 0x0f, 0xa9, 0x1e, 0x7d, 0x05, 0x21, 0x97, 0x15, 0xc4, 0x3d,
	0x83, 0x63, 0xc8, 0xb7, 0x26, 0xfd, 0x87, 0x3c, 0xfe, 0x0f, 0x00, 0x00, 0xff, 0xff, 0xc7, 0x0d,
	0x18, 0xbd, 0x51, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// PairClient is the client API for Pair service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type PairClient interface {
	ElementSave(ctx context.Context, in *EleSaveReq, opts ...grpc.CallOption) (*Response, error)
	ElementView(ctx context.Context, in *EleViewReq, opts ...grpc.CallOption) (*EleViewRsp, error)
	ElementList(ctx context.Context, in *EleListReq, opts ...grpc.CallOption) (*ELeListRsp, error)
	ElementPair(ctx context.Context, in *ElePairReq, opts ...grpc.CallOption) (*ELeListRsp, error)
}

type pairClient struct {
	cc *grpc.ClientConn
}

func NewPairClient(cc *grpc.ClientConn) PairClient {
	return &pairClient{cc}
}

func (c *pairClient) ElementSave(ctx context.Context, in *EleSaveReq, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/pair.Pair/ElementSave", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pairClient) ElementView(ctx context.Context, in *EleViewReq, opts ...grpc.CallOption) (*EleViewRsp, error) {
	out := new(EleViewRsp)
	err := c.cc.Invoke(ctx, "/pair.Pair/ElementView", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pairClient) ElementList(ctx context.Context, in *EleListReq, opts ...grpc.CallOption) (*ELeListRsp, error) {
	out := new(ELeListRsp)
	err := c.cc.Invoke(ctx, "/pair.Pair/ElementList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *pairClient) ElementPair(ctx context.Context, in *ElePairReq, opts ...grpc.CallOption) (*ELeListRsp, error) {
	out := new(ELeListRsp)
	err := c.cc.Invoke(ctx, "/pair.Pair/ElementPair", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PairServer is the server API for Pair service.
type PairServer interface {
	ElementSave(context.Context, *EleSaveReq) (*Response, error)
	ElementView(context.Context, *EleViewReq) (*EleViewRsp, error)
	ElementList(context.Context, *EleListReq) (*ELeListRsp, error)
	ElementPair(context.Context, *ElePairReq) (*ELeListRsp, error)
}

// UnimplementedPairServer can be embedded to have forward compatible implementations.
type UnimplementedPairServer struct {
}

func (*UnimplementedPairServer) ElementSave(ctx context.Context, req *EleSaveReq) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ElementSave not implemented")
}
func (*UnimplementedPairServer) ElementView(ctx context.Context, req *EleViewReq) (*EleViewRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ElementView not implemented")
}
func (*UnimplementedPairServer) ElementList(ctx context.Context, req *EleListReq) (*ELeListRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ElementList not implemented")
}
func (*UnimplementedPairServer) ElementPair(ctx context.Context, req *ElePairReq) (*ELeListRsp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ElementPair not implemented")
}

func RegisterPairServer(s *grpc.Server, srv PairServer) {
	s.RegisterService(&_Pair_serviceDesc, srv)
}

func _Pair_ElementSave_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EleSaveReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PairServer).ElementSave(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pair.Pair/ElementSave",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PairServer).ElementSave(ctx, req.(*EleSaveReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Pair_ElementView_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EleViewReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PairServer).ElementView(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pair.Pair/ElementView",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PairServer).ElementView(ctx, req.(*EleViewReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Pair_ElementList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EleListReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PairServer).ElementList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pair.Pair/ElementList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PairServer).ElementList(ctx, req.(*EleListReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Pair_ElementPair_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ElePairReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PairServer).ElementPair(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/pair.Pair/ElementPair",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PairServer).ElementPair(ctx, req.(*ElePairReq))
	}
	return interceptor(ctx, in, info, handler)
}

var _Pair_serviceDesc = grpc.ServiceDesc{
	ServiceName: "pair.Pair",
	HandlerType: (*PairServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ElementSave",
			Handler:    _Pair_ElementSave_Handler,
		},
		{
			MethodName: "ElementView",
			Handler:    _Pair_ElementView_Handler,
		},
		{
			MethodName: "ElementList",
			Handler:    _Pair_ElementList_Handler,
		},
		{
			MethodName: "ElementPair",
			Handler:    _Pair_ElementPair_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "pair.proto",
}
