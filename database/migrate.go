package database

import (
	"gitee.com/fireflylove/pair-svc/model"
	"gorm.io/gorm"
)

func migrate(db *gorm.DB) {
	db.AutoMigrate(model.Element{})
}
